let { createDir, createFile, deleteFile } = require("../problem1");

createDir("testFirstFile");
createFile("testFirstFile/file1.json", "Hello world!!");
createFile("testFirstFile/file2.json", "Welcome");
createFile("testFirstFile/file3.json", "Enjoy");
createFile("testFirstFile/file4.json", "Have a nice day");

let dltFilesArray = [
  "testFirstFile/file1.json",
  "testFirstFile/file2.json",
  "testFirstFile/file3.json",
  "testFirstFile/file4.json",
];
deleteFile(dltFilesArray, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("deleted all files");
  }
});
