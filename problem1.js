let fs = require("fs");
let path = require("path");

function createDir(dirName) {
  fs.mkdir(path.join(__dirname, dirName), (err) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log("Directory Created");
    }
  });
}

function createFile(filePath, data) {
  fs.writeFile(path.normalize(filePath), data, "utf8", (err) => {
    if (err) {
      console.log(err.message);
    } else {
      console.log("File Created");
    }
  });
}

function deleteFile(files, cb) {
  setTimeout(() => {
    {
      let fileCount = files.length;
      files.map((filepath) => {
        fs.unlink(filepath, (err) => {
          fileCount--;
          if (err) {
            cb(err);
            return;
          } else if (fileCount <= 0) {
            cb(null);
          }
        });
      });
    }
  }, 1000);
}

module.exports = { createDir, createFile, deleteFile };
