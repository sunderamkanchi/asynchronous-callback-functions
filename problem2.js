const fs = require("fs");
const path = require("path");

let problem2 = (textFileData) => {
  function delFiles(delData) {
    delData = delData.trim();
    let finalDel = delData.split("\n");
    let filePath = finalDel.map((filename) => {
      let filePathDel = path.join(__dirname, filename);
      fs.unlink(filePathDel, (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`files deleted`);
        }
      });
    });
    //console.log(filePath);
  }
  function fileNameDel(readFileName) {
    readFileName = "filenames.txt";
    let filePath = path.join(__dirname, readFileName);
    fs.readFile(filePath, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        //console.log(data);
        delFiles(data);
      }
    });
  }

  function storeFileName(nameToAnotherFile) {
    let fileStored = "filenames.txt";
    let filePath = path.join(__dirname, fileStored);
    fs.appendFile(filePath, nameToAnotherFile + "\n", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`${nameToAnotherFile} stored in ${fileStored}`);
      }
    });
  }
  function sortingTwoFiles(sortingdata) {
    // console.log("5", sortingdata);
    let fileName = "sortedfiles.txt";
    let filePath = path.join(__dirname, sortingdata);
    fs.readFile(filePath, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`data readed from ${sortingdata}`);

        let changes = data.split("\n");
        //console.log(changes);
        let finalSort = changes.sort().join("\n").trim();
        //console.log(finalSort);
        fs.writeFile(filePath, finalSort, (err) => {
          if (err) {
            console.log(err);
          } else {
            storeFileName(fileName);
            fileNameDel();
          }
        });
      }
    });
  }

  function datasort(lowerCaseFileName, upperCaseFileName) {
    let lowerFilePath = path.join(__dirname, lowerCaseFileName);

    fs.readFile(lowerFilePath, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`data readed from ${lowerCaseFileName}`);

        let sortedlowerFile = "sortedfiles.txt";
        filePath = path.join(__dirname, sortedlowerFile);

        fs.writeFile(filePath, data, (err) => {
          if (err) {
            console.log(err);
          }
        });
      }
    });

    let upperFilePath = path.join(__dirname, upperCaseFileName);
    fs.readFile(upperFilePath, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`data readed from ${upperCaseFileName}`);

        let sortedlowerFile = "sortedfiles.txt";
        filePath = path.join(__dirname, sortedlowerFile);

        fs.appendFile(filePath, data, (err) => {
          if (err) {
            console.log(err);
          }
          sortingTwoFiles(sortedlowerFile);
        });
      }
    });
  }

  function dataToLowerCase(data, upperCaseFileName) {
    let lowerCaseData = data.toLowerCase();
    let separateSentences = lowerCaseData.split(". ");
    let finalseparation = separateSentences.join(".\n");

    //console.log(lowerCaseData);
    //console.log(separateSentences);
    let lowerCaseFileName = "lipsumSplitToLowerCase.txt";
    let filePath = path.join(__dirname, lowerCaseFileName);
    fs.writeFile(filePath, finalseparation, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`data created in ${lowerCaseFileName}`);
        storeFileName(lowerCaseFileName);
        datasort(lowerCaseFileName, upperCaseFileName);
      }
    });
  }

  function readSecondTextFileData(textLowerFileData) {
    let filePath = path.join(__dirname, textLowerFileData);
    fs.readFile(filePath, "utf-8", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`data readed from ${textLowerFileData}`);
        dataToLowerCase(data, textLowerFileData);
      }
    });
  }

  function dataToUpperCase(data) {
    let upperCaseData = data.toUpperCase();
    //console.log(upperCaseData);
    let fileName = "lipsumUpperCase.txt";
    let filepath = path.join(__dirname, fileName);
    fs.writeFile(filepath, upperCaseData, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`data created in ${fileName}`);
        storeFileName(fileName);
        readSecondTextFileData(fileName);
      }
    });
    4;
  }

  function readTextFileData(textFileData) {
    const filePath = path.join(__dirname, textFileData);
    fs.readFile(filePath, "utf-8", (err, data) => {
      //console.log(data);
      if (err) {
        console.log(err);
      } else {
        console.log(`data from ${textFileData} read`);
        dataToUpperCase(data);
        // return data;
      }
    });
  }
  readTextFileData(textFileData);
};

module.exports = problem2;
